#!/usr/bin/awk

function basename(file, a, n) {
	n = split(file, a, "/")
	 return a[n]
}

BEGIN {
	FS = ","
	elemPos = 1
}
{
	elem[elemPos] = $5
	elemPos++
}
END {
	n = asort(elem)
	ninePercIndex = n * 0.9

	roundValue = ninePercIndex - int(ninePercIndex)

	if (roundValue >= 0.5) {
		ninePercIndex = ninePercIndex + 0.5
	}

	ninePercIndex = int(ninePercIndex)

	total = 0

	for (i=ninePercIndex; i <= n; i++) {
		total += elem[i]
	}

	ninePercAvg = total / (n - ninePercIndex + 1)


	print basename(FILENAME) " " ninePercAvg
}
