# import numpy as np
# import csv
#
# with open("results.csv") as csvfile:
# 	reader = csv.reader(csvfile, delimiter=",")
#         arr = list()
#         for row in reader:
#         	arr.append(float(row[4]))
#         percentile_90 = np.percentile(np.array(arr), 90)
#
# print "\nResponse time: ",
# print percentile_90,
# print "ms"
import csv

with open('results.csv', 'r') as f:
    reader = csv.reader(f)
    arr = list()
    for row in reader:
        arr.append(float(row[4]))
arr.sort()

# print("\nResponse time: " + str(arr[int(len(arr) * 0.9)]) + "ms")
print(str(arr[int(len(arr) * 0.9)]))
