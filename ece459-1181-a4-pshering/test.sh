#!/bin/bash
SERVERS=6
NUM_JOBS=2500
GEN=2
# GEN=(1 2)
ASSIG=(1 2)
LB=(0 1)
LAMBDA=(1000 100 10 1)
MAX_ROUNDS=(15000 12500 10000 7500 5000 2500)


for a in ${ASSIG[@]}; do
    for j in ${NUM_JOBS[@]}; do
        for n in ${SERVERS[@]}; do
            for l in ${LAMBDA[@]}; do
                for m in ${MAX_ROUNDS[@]}; do
                    for b in ${LB[@]}; do
                        ./bin/loadbalance -n $n -a $a -j $j -b $b -l $l -m $m -g $GEN> log
                        r1=$(python percentile.py)
                        ./bin/loadbalance -n $n -a $a -j $j -b $b -l $l -m $m -g $GEN> log
                        r2=$(python percentile.py)
                        ./bin/loadbalance -n $n -a $a -j $j -b $b -l $l -m $m -g $GEN> log
                        r3=$(python percentile.py)
                        # ./bin/loadbalance -n $n -a $a -j $j -b $b -l $l -m $m -g $GEN> log
                        # r4=$(python percentile.py)
                        # ./bin/loadbalance -n $n -a $a -j $j -b $b -l $l -m $m -g $GEN> log
                        # r5=$(python percentile.py)
                        echo $n $a $j $b $l $m "results: " $r1 $r2 $r3
                    done
                done
            done
        done
    done
done
#
#
# for b in ${LB[@]}; do
#     ./bin/loadbalance -j $NUM_JOBS -b $b
#     # ./bin/loadbalance -n $n -a $a -j $j -b $b -l $l -m $m
#     r1=$(python percentile.py)
#     echo $n $a $j $b $l $m "results: " $r1
# done
