#pragma once

static const int OUTPUT_PRECISION = 6; 

extern const float coulombConstant;
extern const float chargeProton;
extern const float chargeElectron;
extern const float massProton;
extern const float massElectron;

