#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>

#include "Constants.h"
#include "Vec3.h"

#define PROTON 'p'
#define ELECTRON 'e'
#define WHICH_CHARGE(p) ( (p) == (PROTON) ? (chargeProton) : (chargeElectron) )

#define SEPARATOR ' '
#define TYPEWIDTH 1
#define NUMWIDTH 13

typedef struct{
    char type;
    Vec3 position;
}particle_t;

/*
    PRINT FUNCTIONS
 */

template<typename T> void printElement(T t, const int& width, bool comma);
void print_particle(std::vector<particle_t> p_t);
void print_d(std::vector<Vec3> d);
void print_d_formatted(std::vector<Vec3> d);

template<typename T> void printElement(T t, const int& width, bool comma) {
    std::cout << std::right << std::setw(width) << std::setfill(SEPARATOR) << std::scientific << std::setprecision(OUTPUT_PRECISION) << t ;
    if (comma) {
        std::cout <<',';
    }
    else std::cout <<std::endl;
}

void print_particle(std::vector<particle_t> p_t) {
    for (int i = 0; i < p_t.size(); ++i)
    {
        printElement(p_t[i].type, TYPEWIDTH, true);
        printElement(p_t[i].position.x, NUMWIDTH, true);
        printElement(p_t[i].position.y, NUMWIDTH, true);
        printElement(p_t[i].position.z, NUMWIDTH, false);
    }
}

void print_d(std::vector<Vec3> d) {
    std::ostream stream(nullptr);
    stream.rdbuf(std::cout.rdbuf());

    for (int i = 0; i < d.size(); ++i) {
        stream << d[i] << std::endl;    
    }
}

void print_d_formatted(std::vector<Vec3> d){
    for (int i = 0; i < d.size(); ++i)
    {
        printElement(d[i].x, NUMWIDTH, true);
        printElement(d[i].y, NUMWIDTH, true);
        printElement(d[i].z, NUMWIDTH, false);
    }
}

/*
    FILE OUTPUT
 */

void write_to_file(std::string filename, std::vector<particle_t> p_out);

void write_to_file(std::string filename, std::vector<particle_t> p_out) {
    std::ofstream f;
    filename = filename.replace(filename.length()-2, 2, "out");
    f.open(filename);

    f.setf(std::ios::right);
    f.setf(std::ios::showpoint);
    f.setf(std::ios::scientific);
    f.precision(OUTPUT_PRECISION);

    for (int i = 0; i < p_out.size(); ++i)
    {
        f << std::setw(TYPEWIDTH) << p_out[i].type << ',' 
          << std::setw(NUMWIDTH) << p_out[i].position.x << ',' 
          << std::setw(NUMWIDTH) << p_out[i].position.y << ','
          << std::setw(NUMWIDTH) << p_out[i].position.z << '\n';
    }

    f.close();

    std::cout << " done writting to file" << std::endl;
}

/*
    CALCULATION FUNCTIONS
 */

std::vector<particle_t>* huns_method2(std::vector<particle_t> *p_in, const float h, const float e);
std::vector<particle_t>* propogate_position(std::vector<particle_t> *particle_init_pos, std::vector<Vec3> *k_forces, std::vector<Vec3> *d, const float h);
std::vector<particle_t>* ave_position(std::vector<particle_t> *particle_init_pos, std::vector<particle_t> *particle_final_pos, std::vector<particle_t> *particle_ave_pos, std::vector<Vec3> *d0, std::vector<Vec3> *d1, const float e, bool *success);

std::vector<particle_t>* huns_method2(std::vector<particle_t> *p_in, const float h, const float e) {
    // std::cout << "h:" << h << "\n" << std::endl;
    std::vector<Vec3> * k0_forces = new std::vector<Vec3>((*p_in).size());
    std::vector<Vec3> * d0 = new std::vector<Vec3>((*p_in).size());
    std::vector<Vec3> * k1_forces = new std::vector<Vec3>((*p_in).size());
    std::vector<Vec3> * d1 = new std::vector<Vec3>((*p_in).size());
    bool success = true;

    std::vector<particle_t> *particle_pos1 = propogate_position(p_in, k0_forces, d0, h);
    std::vector<particle_t> *particle_pos2 = propogate_position(particle_pos1, k1_forces, d1, h);
    std::vector<particle_t> *particle_pos_ave = ave_position(p_in, particle_pos1, particle_pos2, d0, d1, e, &success);

    if (!success) {
        return huns_method2(p_in, h/2, e);
    }
    else {
        return particle_pos_ave;
    }
}

std::vector<particle_t> * propogate_position(std::vector<particle_t> *particle_init_pos, std::vector<Vec3> *k_forces, std::vector<Vec3> *d, const float h) {
    const float h2m = h*h / massElectron;

    std::vector<particle_t> * particle_final_pos = new std::vector<particle_t>((*particle_init_pos).size());
        for (int i = 0; i < particle_final_pos->size(); ++i) {
            for (int j = 0; j < (*particle_init_pos).size(); ++j) {
                if ( ((*particle_init_pos)[i].type == ELECTRON) && (i != j) )
                {   
                    Vec3 temp = ((*particle_init_pos)[i].position - (*particle_init_pos)[j].position);
                    (*k_forces)[i] += ( (temp.normal()) * 
                        ( (coulombConstant * WHICH_CHARGE((*particle_init_pos)[i].type) * WHICH_CHARGE((*particle_init_pos)[j].type) ) / (temp.magnitude()*temp.magnitude()) ) ) ;
                }
            }
            (*particle_final_pos)[i].type = (*particle_init_pos)[i].type;
            (*d)[i] = (*k_forces)[i] * (h2m);
            (*particle_final_pos)[i].position = (*particle_init_pos)[i].position + (*d)[i];
        }
    return particle_final_pos;
}

std::vector<particle_t> * ave_position(std::vector<particle_t> *particle_init_pos, std::vector<particle_t> *particle_final_pos, std::vector<particle_t> *particle_ave_pos, std::vector<Vec3> *d0, std::vector<Vec3> *d1, const float e, bool *success) {
    Vec3 diff;
    *success = true;

    for (int i = 0; i < (*particle_ave_pos).size(); ++i) {
        (*particle_ave_pos)[i].position = (*particle_init_pos)[i].position + ( ( (*d0)[i] + (*d1)[i]) / 2);
        diff = (*particle_ave_pos)[i].position - (*particle_final_pos)[i].position;
        if (diff.magnitude() > e) {
            *success = false;
            break;
        }
    }
    return particle_ave_pos;
}

/*
    MAIN
 */
int main(int argc, const char *argv[]) {
    if (argc != 4) {
        std::cerr << "Expected 3 arguments but got " << (argc - 1) << std::endl;
        exit(1);
    }

    const float h = std::stof(argv[1]);
    const float e = std::stof(argv[2]);
    std::string filename = argv[3];
    // std::cout << argv[0] << "has args h: " << h << " e:" << e <<  " file_location:" << filename << std::endl;

    std::vector<particle_t> p_in;

    // reading file
    particle_t temp;
    FILE* file = fopen(filename.c_str(), "r");
    if (file != NULL) {
        while(fscanf(file, "%c,%f,%f,%f\n", &temp.type, &temp.position.x, &temp.position.y, &temp.position.z)==4) {
            p_in.push_back(temp);
        }
    } else {
        std::cerr << "Unable to open file" << std::endl;
        exit(1);
    }
    std::vector<particle_t> *p = &p_in;

    std::vector<particle_t> * p_out;
    p_out = huns_method2(p, h, e);

    write_to_file(filename, *p_out);

    return 0;
}
