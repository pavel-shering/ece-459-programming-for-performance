#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>

#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>

#define PROTON 'p'
#define ELECTRON 'e'
#define WHICH_CHARGE(p) ( (p) == (PROTON) ? (chargeProton) : (chargeElectron) )

#define SEPARATOR ' '
#define TYPEWIDTH 1
#define NUMWIDTH 13
static const int OUTPUT_PRECISION = 5; 

/*
    PRINT FUNCTIONS
 */

template<typename T> void printElement(T t, const int& width, bool comma);
void print_particle(std::vector<cl_char> type, std::vector<cl_float3> p_t);
void print_d(std::vector<cl_float3> d);

template<typename T> void printElement(T t, const int& width, bool comma) {
    std::cout << std::right << std::setw(width) << std::setfill(SEPARATOR) << std::scientific << std::setprecision(OUTPUT_PRECISION) << t ;
    if (comma) {
        std::cout <<',';
    }
    else std::cout <<std::endl;
}

void print_particle(std::vector<cl_char> type, std::vector<cl_float3> p_t) {
    for (int i = 0; i < p_t.size(); ++i)
    {
        printElement(type[i], TYPEWIDTH, true);
        printElement(p_t[i].x, NUMWIDTH, true);
        printElement(p_t[i].y, NUMWIDTH, true);
        printElement(p_t[i].z, NUMWIDTH, false);
    }
}

void print_d(std::vector<cl_float3> d){
    for (int i = 0; i < d.size(); ++i)
    {
        // std::cout<< "i: " << i << std::endl;
        printElement(d[i].x, NUMWIDTH, true);
        printElement(d[i].y, NUMWIDTH, true);
        printElement(d[i].z, NUMWIDTH, false);
    }
}

/*
    FILE OUTPUT
 */

void write_to_file(std::string filename, std::vector<cl_char> type, std::vector<cl_float3> p_out);

void write_to_file(std::string filename, std::vector<cl_char> type, std::vector<cl_float3> p_out) {
    std::ofstream f;
    filename = filename.replace(filename.length()-2, 2, "out");
    f.open(filename);

    f.setf(std::ios::right);
    f.setf(std::ios::showpoint);
    f.setf(std::ios::scientific);
    f.precision(OUTPUT_PRECISION);

    for (int i = 0; i < p_out.size(); ++i)
    {
        f << std::setw(TYPEWIDTH) << type[i] << ',' 
          << std::setw(NUMWIDTH) << p_out[i].x << ',' 
          << std::setw(NUMWIDTH) << p_out[i].y << ','
          << std::setw(NUMWIDTH) << p_out[i].z << '\n';
    }

    f.close();

    std::cout << " done writing to file" << std::endl;
}


int main(int argc, const char *argv[]) {
    if (argc != 4) {
        std::cerr << "Expected 3 arguments but got " << (argc - 1) << std::endl;
        exit(1);
    }
    cl_float h = std::stof(argv[1]);
    cl_float e = std::stof(argv[2]);
    std::string filename = argv[3];
    // std::cout << argv[0] << "has args h: " << h << " e:" << e <<  " file_location:" << filename << std::endl;

    std::vector<cl_char> type;
    std::vector<cl_float3> p_in;

    // reading file
    cl_char t;
    cl_float3 temp;
    FILE* file = fopen(filename.c_str(), "r");
    if (file != NULL) {
        while(fscanf(file, "%c,%f,%f,%f\n", &t, &temp.x, &temp.y, &temp.z)==4) {
            type.push_back(t);
            p_in.push_back(temp);
        }
    } else {
        std::cerr << "Unable to open file" << std::endl;
        exit(1);
    }

    // print_particle(type, p_in);

    try { 
        // Get available platforms
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);

        // Select the default platform and create a context using this platform and the GPU
        cl_context_properties cps[3] = { 
            CL_CONTEXT_PLATFORM, 
            (cl_context_properties)(platforms[0])(), 
            0 
        };
        cl::Context context(CL_DEVICE_TYPE_GPU, cps);
 
        // Get a list of devices on this platform
        std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
 
        // Create a command queue and use the first device
        cl::CommandQueue queue = cl::CommandQueue(context, devices[0]);
 
        // Read source file
        std::ifstream sourceFile("protons.cl");
            if(!sourceFile.is_open()){
                std::cerr << "Cannot find kernel file" << std::endl;
                throw;
            }
        std::string sourceCode(std::istreambuf_iterator<char>(sourceFile), (std::istreambuf_iterator<char>()));
        cl::Program::Sources source(1, std::make_pair(sourceCode.c_str(), sourceCode.length()+1));
 
        // Make program of the source code in the context
        cl::Program program = cl::Program(context, source);
 
        // Build program for these specific devices
        try {
            program.build(devices);
        } catch(cl::Error error) {
            std::cerr << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << std::endl;
            throw;
        }
 
        // Make kernels
        // cl::Kernel kernel(program, "KERNEL_NAME_GOES_HERE");
        cl::Kernel kernel_pos0(program, "particle_position");
        cl::Kernel kernel_pos1(program, "particle_position");
        cl::Kernel kernel_avepos(program, "ave_position");
 
        // Create buffers and write to them
        cl::Buffer typeBuf(
            context,
            CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
            type.size()*sizeof(cl_char),
            type.data()
        );

        cl::Buffer p_inBuf(
            context,
            CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
            p_in.size()*sizeof(cl_float3),
            p_in.data()
        );

        cl::Buffer k0Buf(
            context,
            CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl::Buffer d0Buf(
            context,
            CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl::Buffer k1Buf(
            context,
            CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl::Buffer d1Buf(
            context,
            CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl::Buffer p1Buf(
            context,
            CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl::Buffer p2Buf(
            context,
            CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl::Buffer p_aveBuf(
            context,
            CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
            p_in.size()*sizeof(cl_float3)
        );

        cl_int *success = new cl_int();
        *success = 1;
        cl::Buffer successBuf(
            context,
            CL_MEM_READ_WRITE,
            sizeof(cl_int),
            success
        );

        cl::Buffer hBuf(
            context,
            CL_MEM_READ_WRITE,
            sizeof(cl_int),
            &h
        );

        // Write buffers

        // Set arguments to kernel
        kernel_pos0.setArg(0, hBuf);
        kernel_pos0.setArg(1, typeBuf);
        kernel_pos0.setArg(2, p_inBuf);
        kernel_pos0.setArg(3, p1Buf);
        kernel_pos0.setArg(4, k0Buf);
        kernel_pos0.setArg(5, d0Buf);

        kernel_pos1.setArg(0, hBuf);
        kernel_pos1.setArg(1, typeBuf);
        kernel_pos1.setArg(2, p1Buf);
        kernel_pos1.setArg(3, p2Buf);
        kernel_pos1.setArg(4, k1Buf);
        kernel_pos1.setArg(5, d1Buf);

        kernel_avepos.setArg(0, e);
        kernel_avepos.setArg(1, p_inBuf);
        kernel_avepos.setArg(2, p1Buf);
        kernel_avepos.setArg(3, p_aveBuf);
        kernel_avepos.setArg(4, d0Buf);
        kernel_avepos.setArg(5, d1Buf);
        kernel_avepos.setArg(6, successBuf);

        // Run the kernel on specific ND range
        cl::NDRange global(type.size());
        // cl::NDRange local(1);

        while((*success))
        {
            queue.enqueueWriteBuffer(
                hBuf,
                CL_TRUE,
                0,
                sizeof(cl_int),
                &h
            );

            queue.enqueueNDRangeKernel(
                kernel_pos0,
                cl::NullRange,
                global
                // global,
                // local
            );
            
            // std::cout << "h=" << h << std::endl;
            // std::vector<cl_float3> *check_pout1 = new std::vector<cl_float3>(p_in.size());
            // queue.enqueueReadBuffer(
            //     p1Buf,
            //     CL_TRUE,
            //     0,
            //     p_in.size()*sizeof(cl_float3),
            //     (*check_pout1).data()
            // );

            // std::vector<cl_float3> *check_k0 = new std::vector<cl_float3>(p_in.size());
            // queue.enqueueReadBuffer(
            //     k0Buf,
            //     CL_TRUE,
            //     0,
            //     p_in.size()*sizeof(cl_float3),
            //     (*check_k0).data()
            // );

            // std::vector<cl_float3> *check_d0 = new std::vector<cl_float3>(p_in.size());
            // queue.enqueueReadBuffer(
            //     d0Buf,
            //     CL_TRUE,
            //     0,
            //     p_in.size()*sizeof(cl_float3),
            //     (*check_d0).data()
            // );

            queue.enqueueNDRangeKernel(
                kernel_pos1,
                cl::NullRange,
                global
                // global,
                // local
            );

            // std::vector<cl_float3> *check_pout2 = new std::vector<cl_float3>(p_in.size());
            // queue.enqueueReadBuffer(
            //     p2Buf,
            //     CL_TRUE,
            //     0,
            //     p_in.size()*sizeof(cl_float3),
            //     (*check_pout2).data()
            // );

            // std::vector<cl_float3> *check_k1 = new std::vector<cl_float3>(p_in.size());
            // queue.enqueueReadBuffer(
            //     k1Buf,
            //     CL_TRUE,
            //     0,
            //     p_in.size()*sizeof(cl_float3),
            //     (*check_k1).data()
            // );

            // std::vector<cl_float3> *check_d1 = new std::vector<cl_float3>(p_in.size());
            // queue.enqueueReadBuffer(
            //     d1Buf,
            //     CL_TRUE,
            //     0,
            //     p_in.size()*sizeof(cl_float3),
            //     (*check_d1).data()
            // );

            *success = 0;
            queue.enqueueWriteBuffer(
                successBuf,
                CL_TRUE,
                0,
                sizeof(cl_int),
                success
            );

            queue.enqueueNDRangeKernel(
                kernel_avepos,
                cl::NullRange,
                global
                // global,
                // local
            );

            queue.enqueueReadBuffer(
                successBuf,
                CL_TRUE,
                0,
                sizeof(cl_int),
                success
            );

            h /= 2.0f;
        }
        
        // Read buffer(s)
        std::vector<cl_float3> *z1 = new std::vector<cl_float3>(p_in.size());
        queue.enqueueReadBuffer(
            p_aveBuf,
            CL_TRUE,
            0,
            p_in.size()*sizeof(cl_float3),
            (*z1).data()
        );

        // std::cout << '\n' << std::endl;

        // print_particle(type,*z1);
        write_to_file(filename, type, *z1);

    } catch(cl::Error error) {
        std::cout << error.what() << "(" << error.err() << ")" << std::endl;
    }
 
    return EXIT_SUCCESS;
}
