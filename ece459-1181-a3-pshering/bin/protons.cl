#define coulombConstant 8.99 * pow(10.0f, 9.0f)
#define chargeProton    1.60217662 * pow(10.0f, -19.0f)
#define chargeElectron  -chargeProton
#define massProton      1.67262190 * pow(10.0f, -27.0f)
#define massElectron    9.10938356 * pow(10.0f, -31.0f)

// #define EPS 1e-10

#define PROTON 'p'
#define ELECTRON 'e'
#define WHICH_CHARGE(p) ( (p) == (PROTON) ? (chargeProton) : (chargeElectron) )

float magnitude(float3 a) {
    return sqrt( (float)(
        pow(a.x, 2.0f) +
        pow(a.y, 2.0f) +
        pow(a.z, 2.0f) )
    );
}

float3 normal(float3 a) {
    float factor = 1.0f / magnitude(a);
    return a * factor;
}

__kernel void particle_position(__global float *h, 
                                __global const char *p_type, 
                                __global const float3 *p, 
                                __global float3 *p_out, 
                                __global float3 *k_forces, 
                                __global float3 *d) 
{
    int i = get_global_id(0);

    float h2m = (float)((float)((*h)*(*h)) / (float)(massElectron));
    k_forces[i] = (float3){0.0f, 0.0f, 0.0f}; 
    p_out[i] = p[i];

    if(p_type[i] == ELECTRON)
    {
        for (int j = 0; j < get_global_size(0); ++j)
        {
            if(j == i )
            {
                continue;
            }else
            {
                float3 temp = p[i] - p[j];
                float mag = magnitude(temp);
                float3 norm = normal(temp);
                k_forces[i] += (float3)( norm * 
                            (float)(( (coulombConstant * WHICH_CHARGE(p_type[i]) * WHICH_CHARGE(p_type[j]) ) / (float) (mag*mag) ) )) ;
            }
        }
    }
    // k_forces[i] = force;
    d[i] = k_forces[i] * (h2m);
    p_out[i] += d[i];
}

__kernel void ave_position( const float e, 
                            __global const float3 *p_init, 
                            __global float3 *p_out, 
                            __global float3 *p_ave,
                            __global float3 *d0,
                            __global float3 *d1,
                            __global int *success)
{
    int i = get_global_id(0);

    p_ave[i] = p_init[i] + (float3)( (float3)(d0[i] + d1[i]) / 2.0f);
    float mag = magnitude(p_ave[i] - p_out[i]);
    if( mag > e) {
        *success = 1;
    }
}
