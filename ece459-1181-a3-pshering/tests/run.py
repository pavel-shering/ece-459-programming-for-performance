#!/usr/bin/python3


import os
import subprocess


EXEC = '../bin/protons_omp'
H = 0.001
E = 0.00001


if __name__ == '__main__':
    for f in os.listdir('.'):
        if not f.endswith(".in"):
            continue

        fileName = f[:-(len('.in'))]
        inputFile = fileName + '.in'
        outputFile = fileName + '.out'
        argv = ['time', EXEC, str(H), str(E), inputFile]

        with open(outputFile, 'w') as out:
            subprocess.call(argv, stdout=out)
